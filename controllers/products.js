const Sequelize = require('sequelize');
const Product = require('../models/products');

exports.getAllProducts = async (req, res, next) => {
    console.log('get all products');
    const allProducts = await Product.findAll();
    try {
        if (!allProducts) {
            const error = new Error('Could not find any product.');
            error.statusCode = 404;
            throw error;
        }

        res.status(200).json(allProducts);

    } catch (error) {
        if (!error.statusCode) {
            error.statusCode = 500;
        }
        next(error);
    }
};

exports.getSingleProduct = async (req, res, next) => {
    const productId = req.params.productId;
    try {
        const product = await Product.findOne({
            where: {
                productId: productId,
            },

        });
        if (!product) {
            const error = new Error('Could not find product with id .' + productId);
            error.statusCode = 404;
            throw error;
        }

        res.status(200).send(product);
    } catch (error) {
        if (!error.statusCode) {
            error.statusCode = 500;
        }
        next(error);
    }
};

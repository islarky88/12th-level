
const Sequelize = require('sequelize');
const UserProduct = require('../models/userProducts');

exports.getAllUserProducts = async (req, res, next) => {
    const allUserProducts = await UserProduct.findAll();
    try {
        if (!allUserProducts) {
            const error = new Error('Could not any userProducts.');
            error.statusCode = 404;
            throw error;
        }

        res.status(200).json(allUserProducts);
    } catch (error) {
        if (!error.statusCode) {
            error.statusCode = 500;
        }
        next(error);
    }
};

exports.getSingleUserProduct = async (req, res, next) => {
    const userProductId = req.params.userProductId;
    try {
        const userProduct = await UserProduct.findOne({
            where: {
                userProductId: userProductId,
            },

        });
        if (!userProduct) {
            const error = new Error('Could not user products with this id: ' + userId);
            error.statusCode = 404;
            throw error;
        }

        res.status(200).send(userProduct);
    } catch (error) {
        if (!error.statusCode) {
            error.statusCode = 500;
        }
        next(error);
    }
};

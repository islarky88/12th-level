
const Sequelize = require('sequelize');
const User = require('../models/users');

exports.getAllUsers = async (req, res, next) => {
    const allUsers = await User.findAll();
    try {
        if (!allUsers) {
            const error = new Error('Could not any users.');
            error.statusCode = 404;
            throw error;
        }

        res.status(200).json(allUsers);
    } catch (error) {
        if (!error.statusCode) {
            error.statusCode = 500;
        }
        next(error);
    }
};

exports.getSingleUser = async (req, res, next) => {
    const userId = req.params.userId;
    try {
        const user = await User.findOne({
            where: {
                userId: userId,
            },

        });
        if (!user) {
            const error = new Error('Could not find this user with id .' + userId);
            error.statusCode = 404;
            throw error;
        }

        res.status(200).send(user);
    } catch (error) {
        if (!error.statusCode) {
            error.statusCode = 500;
        }
        next(error);
    }
};

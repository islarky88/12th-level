const express = require('express');

const userProductsController = require('../controllers/userProducts');

const router = express.Router();

// Get All User Products
router.get('/all', userProductsController.getAllUserProducts);

// Get Single User Product
router.get('/:userProductId', userProductsController.getSingleUserProduct);


module.exports = router;

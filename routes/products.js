const express = require('express');

const productsController = require('../controllers/products');

const router = express.Router();

// Get All Products
router.get('/all', productsController.getAllProducts);

// Get Single Product
router.get('/:productId', productsController.getSingleProduct);


module.exports = router;

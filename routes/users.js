const express = require('express');

const usersController = require('../controllers/users');

const router = express.Router();

// Get All Users
router.get('/all', usersController.getAllUsers);

// Get Single User
router.get('/:userId', usersController.getSingleUser);


module.exports = router;

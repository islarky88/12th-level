const Sequelize = require('sequelize');
const sequelize = require('../utils/database');

const User = sequelize.define('users', {
  userId: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true,
  },
  name: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  // createdBy: {
  //   type: Sequelize.STRING,
  //   allowNull: true,
  // },
  // updatedBy: {
  //   type: Sequelize.STRING,
  //   allowNull: true,
  // },
});

module.exports = User;

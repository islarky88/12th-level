const Sequelize = require('sequelize');
const sequelize = require('../utils/database');

const UserProducts = sequelize.define('userProducts', {
  userProductId: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true,
  },
  userId: {
    type: Sequelize.INTEGER,
    allowNull: false,
  },
  productId: {
    type: Sequelize.INTEGER,
    allowNull: false,
  },
  Quantity: {
    type: Sequelize.INTEGER,
    allowNull: false,
  },
  // createdBy: {
  //   type: Sequelize.STRING,
  //   allowNull: true,
  // },
  // updatedBy: {
  //   type: Sequelize.STRING,
  //   allowNull: true,
  // },
});

module.exports = UserProducts;

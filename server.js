const dotenv = require('dotenv');
const express = require('express');
const bodyParser = require('body-parser');

dotenv.config();
const app = express();

// Routes
const userRoutes = require('./routes/users');
const productRoutes = require('./routes/products');
const userProductRoutes = require('./routes/userProducts');

// Models
const User = require('./models/users');
const Product = require('./models/products');
const UserProduct = require('./models/userProducts');


// Disable Certain Headers
app.disable('x-powered-by');

// Parse data
app.use(bodyParser.json()); // application/json


// CORS header
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader(
        'Access-Control-Allow-Methods',
        'OPTIONS, GET, POST, PUT, PATCH, DELETE',
    );
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    next();
});

// API Routes
app.use('/api/users', userRoutes);
app.use('/api/products', productRoutes);
app.use('/api/userProducts', userProductRoutes);

// API error catcher
app.use((error, req, res, next) => {
    console.log('ERRORS FOUND: ' + error.message);
    const status = error.statusCode || 500;
    const message = error.message;
    const data = error.data;
    res.status(status).json({ error: message, data: data });
});

// Not found error
app.use((req, res) => {
    res.status(404).json({ error: "not found" });
});

// Initialize Express Server
app.listen(process.env.PORT, () =>
    console.log(`Go to http://localhost:${process.env.PORT} to access API!`),
);
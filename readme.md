# 12th Level Backend Node Express

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run start
```

#### port 3000 for localhost for API
```
http://localhost:3000/api/
```

##### API routes
```
http://localhost:3000/api/users/all
http://localhost:3000/api/users/1
http://localhost:3000/api/products/all
http://localhost:3000/api/products/1
http://localhost:3000/api/userProducts/all
http://localhost:3000/api/userProducts/1
```

##### Postman Collection
```
https://www.getpostman.com/collections/0e5b49cd12f8b0eca211
```